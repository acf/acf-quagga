<% local data, viewlibrary, page_info, session = ...
html = require("acf.html")
htmlviewfunctions = require("htmlviewfunctions")
%>

<% local header_level = htmlviewfunctions.displaysectionstart(data.value.showip, page_info) %>
<pre>
<%= html.html_escape(tostring(data.value.showip.value)) %>
</pre>
<% htmlviewfunctions.displaysectionend(header_level) %>
