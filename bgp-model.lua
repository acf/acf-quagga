local mymodule = {}

-- Load libraries
modelfunctions = require("modelfunctions")
format = require("acf.format")
socket = require("socket")

-- Set variables
local path="PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin "

local configfile = "/etc/quagga/bgpd.conf"
local processname = "bgpd"
local packagename = "quagga"
local portnumber = 2605

-- ################################################################################
-- LOCAL FUNCTIONS
local function parseconfigfile()
	local conf = {}
	local f = io.open(configfile, "r")
	local line, key, _, k, v

	if not f then
		return nil
	end

	for line in f:lines() do
		line = string.gsub(line, "%s*#.*", "")
		local k,v = string.match(line, "^%s*(%S*)%s+(.*)")
		if k then
			conf[k] = v
		end
	end
	f:close()
	return conf
end

-- Use socket.protect to create a function that will not throw exceptions and cleans itself up
local send_to_event_socket = socket.protect(function(cmd)
	-- connect to freeswitch
	local conn = socket.try(socket.connect("127.0.0.1", portnumber))
	conn:settimeout(1) -- timeout of 1 second for response to each command
	-- create a try function that closes 'conn' on error
	local try = socket.newtry(function() conn:close() end)
	-- do everything reassured conn will be closed
	local out = {}
	local o
	repeat
		o,e = conn:receive()
		out[#out+1] = o
	until o == nil
	for i,c in ipairs(cmd) do
		try(conn:send(c.."\n"))
		repeat
			o,e = conn:receive()
			out[#out+1] = o
		until o == nil
	end
	conn:close()
	return table.concat(out, "\n") or ""
end)

local function telnetshowipbgp()
	local output = {}
	local configfile = parseconfigfile() or {}
	local cmd = {configfile.password, "show ip bgp", "quit"}
	local result = send_to_event_socket(cmd)
	if not result or result == "" then
		result = "Failed to find routes"
	end
	local startout, stopout
	for line in string.gmatch(result, "([^\n]*)\n?") do
		table.insert(output,line)
		if (string.find(line, "^Password:")) then
			startout = #output+1
		elseif (string.find(line, "^BGP")) then
			startout = #output
		elseif (string.find(line, "^Total number")) then
			stopout = #output
		end
	end
	return table.concat(output,"\n",startout,stopout)
end

-- ################################################################################
-- PUBLIC FUNCTIONS

function mymodule.getstatus()
	return modelfunctions.getstatus(processname, packagename, "BGP Status")
end

function mymodule.getconfigfile()
	return modelfunctions.getfiledetails(configfile)
end

function mymodule.setconfigfile(self, filedetails)
	return modelfunctions.setfiledetails(self, filedetails, {configfile})
end

function mymodule.get_startstop(self, clientdata)
        return modelfunctions.get_startstop(processname)
end

function mymodule.startstop_service(self, startstop, action)
        return modelfunctions.startstop_service(startstop, action)
end

function mymodule.getdetails()
	local status = {}
	status.showipbgp = cfe({ label="BGP routes" })
	status.showipbgp.value = telnetshowipbgp()
	return cfe({ type="group", value=status, label="BGP Details" })
end

return mymodule
